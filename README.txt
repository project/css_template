
CSS Template allows theme developers to add user configurable values to a theme's CSS files.  This module was based on the Color module.

In order to enable a theme to use CSS Template, the author must define the configuration include 'css_template/css_template.inc' within the theme folder.  An example css_template.inc that demonstrates all of the available variable types would be:

<?php
$info = array(
  'templates' => array(
    'mytheme.css',
  ),
  'variables' => array(
    'text_color' => array(
      'type' => 'color',
      'title' => 'Text Color',
    ),
    'background_image' => array(
      'type' => 'image',
      'title' => 'Background Image',
      'maximum_dimensions' => '570x324', // optional
      'minimum_dimensions' => '100x100', // optional
    ),
    'container_width' => array(
      'type' => 'length',
      'title' => 'Container Width',
    ),
    'body_font' => array(
      'type' => 'options',
      'title' => 'Body Font',
      'options' => array(
        'Verdana, Arial, sans-serif' => 'Sans-serif',
        'Georgia, serif' => 'Serif',
      ),
    ),
    'misc_text' => array(
      'type' => 'text',
      'title' => 'Misc Unvalidated Text',
    ),
  ),
);

These variables will be presented on the theme configuration page.  When set, the theme CSS files specified in $info['templates'] will be updated with the given values.  In order for these updates to work the appropriate comment markers must be inserted into the CSS files.  For example:

#container {
  background-image:/*background_image*/none/*background_image*/;
  width:/*container_width*//*background_image.width*/100%/*background_image.width*//*container_width*/;
  margin:auto;
}

#body {
  font-family:/*body_font*/Verdana,Arial,Helvetica,sans-serif/*body_font*/;
  color:/*text_color*/#555555/*text_color*/;
}

These variable comment markers must be in pairs, with the inner text being the default value if no value is given on the theme configuration page or if the module is not enabled.  The markers can also be nested, with inner pairs only being evaluated if the immediate outer pair had no value defined.

Image type variables also make the uploaded file's dimensions available via 'width' and 'height' dot properties, as demonstrated in the example CSS above.

